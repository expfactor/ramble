FROM python:3.10
RUN apt-get update
RUN apt-get install -y iputils-ping mpv chromium timelimit wget wakeonlan vlc vlc-plugin-access-extra xvfb i3-wm xserver-xephyr xnest xvfb libglu1-mesa-dev #mkchromecast
RUN mkdir -p /opt/ramble
WORKDIR  /opt/ramble
COPY . .
RUN pip install -r requirements.txt && pip install -U  PyChromecast  && pip install -U catt
RUN python setup.py install
USER 1000
ENV RAMBLE_CHROMIUM_PATH=/usr/bin/chromium
ENTRYPOINT ramble

# ############################################################################
# |L|I|C|E|N|S|E|L|I|C|E|N|S|E|L|I|C|E|N|S|E|L|I|C|E|N|S|E|
# Copyright (c) Bertrand Nouvel.
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
# 3. Neither the name of the University nor the names of its contributors
#    may be used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.
# |P|R|O|G|R|A|M|P|R|O|G|R|A|M|P|R|O|G|R|A|M|P|R|O|G|R|A|M|
# ############################################################################
import functools
import os

import aiohttp
import yaml
from async_lru import alru_cache
from dynaconf import settings as config


# @functools.lru_cache(32)
@alru_cache(maxsize=32)
async def _get_playlist_content(location):
    if location.startswith("http://") or location.startswith("https://"):
        connector = aiohttp.TCPConnector(
            verify_ssl=not config["ignore_certificate_errors"]
        )
        async with aiohttp.client.ClientSession(connector=connector) as c:
            content = await (await c.get(location)).read()
    else:
        content = open(location, "r")
    return yaml.safe_load(content)


async def load_profile(profile):
    location = os.path.expanduser(
        config.get("playlist_location").format(profile=profile)
    )
    return await _get_playlist_content(location)


async def load_playlist(profile, playlist):
    pc = await load_profile(profile)
    pl = pc["playlists"][playlist]
    if isinstance(pl, list):
        pl = {"strategy": "random", "entries": pl}
    return pl


# def more(entry):
# find the entry in the playlist
# increase or decrese its weight inside this playlist
#    entry["playlist"]

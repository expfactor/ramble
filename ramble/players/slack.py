# ############################################################################
# |L|I|C|E|N|S|E|L|I|C|E|N|S|E|L|I|C|E|N|S|E|L|I|C|E|N|S|E|
# Copyright (c) Bertrand Nouvel.
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
# 3. Neither the name of the University nor the names of its contributors
#    may be used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.
# |P|R|O|G|R|A|M|P|R|O|G|R|A|M|P|R|O|G|R|A|M|P|R|O|G|R|A|M|
# ############################################################################
import asyncio
import os
import json
from slack import WebClient
from slack.errors import SlackApiError
from dynaconf import settings as config
import tempfile
from .chromium import Browser


class SlackPlayer:
    """Send a picture or a videolink to slack."""

    def __init__(self, ncontents=1):
        self.tempdir = tempfile.TemporaryDirectory()
        self.directory = self.tempdir.name
        self.curcontentidx = 0
        self.ncontents = ncontents

        self.browser = Browser(
            headless=True,
            nimages=3,
            screenshot_dir=f"{self.directory}/{self.curcontentidx}",
        )
        self.browser_entered = False

    async def __aenter__(self):
        self.client = WebClient(token=config.get("slack_api_token"), run_async=True)

        self.directory = self.directory or ramble_offline_profile_path(
            config["profile"]
        )
        if not os.path.exists(self.directory):
            os.makedirs(self.directory)
        if not self.browser_entered:
            await self.browser.__aenter__()

        return self

    async def __aexit__(self, *args):
        if self.browser_entered:
            await self.browser1.__aexit__(*args)
        return False

    async def navigate(self, entry):
        url = entry["url"]
        timelimit = entry.get("duration")

        if not os.path.isdir(f"{self.directory}/{self.curcontentidx}"):
            os.mkdir(f"{self.directory}/{self.curcontentidx}")

        open(f"{self.directory}/{self.curcontentidx}/entry.json", "w").write(
            json.dumps(entry)
        )
        self.browser.screenshot_dir = f"{self.directory}/{self.curcontentidx}"

        await self.browser.navigate(entry)
        await asyncio.sleep(2)

        filepath = f"{self.directory}/{curcontentidx}/img0.png"
        response = self.client.files_upload(
            channels=config.get("slack_channel"),
            file=filepath,
            file_type="jpg"
            # files_remote_add
            # external_url=
            # preview_image=
        )

        print(response)

        return True

    async def playing(self, entry):
        return False


Player = SlackPlayer

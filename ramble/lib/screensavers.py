from ramble.utils import ramble_path, ramble_profile_path


def screensaver_install(screensaver_type="kscreenlocker", profile=None):
    """
    Configure a screensaver to show the images produced by ramble when online

    :param profile:
    :return:
    """
    if screensaver_type == "kscreenlocker":
        if not os.path.exists(os.path.expanduser("~/.config/kscreenlockerrc")):
            raise NotImplementedError(
                "Please install and configure kscreenlockerrc before we can install ramble-screensaver"
            )
        if not os.path.exists(os.path.expanduser("~/.config/kscreenlockerrc.offline")):
            shutil.copy(
                os.path.expanduser("~/.config/kscreenlockerrc"),
                os.path.expanduser("~/.config/kscreenlockerrc.offline"),
            )
        connected = "default"
        if os.path.exists(os.path.join(ramble_path(), "get_connection_profile.sh")):
            subprocess.Popen()
            os.system(os.path.join(ramble_path(), "get_connection_profile.sh"))
        if connected:
            profile = connected
            kscreensaver = f"""
            [$Version]
            update_info=kscreenlocker.upd:0.1-autolock

            [Greeter]
            WallpaperPlugin=org.kde.slideshow

            [Greeter][Wallpaper][org.kde.slideshow][General]
            SlideInterval=10
            SlidePaths={os.path.join(ramble_profile_path(profile), "screensaver")}
            TransitionAnimationDuration=0.001
            """
            open(os.path.expanduser("~/.config/kscreenlockerrc"), "w").write(
                textwrap.dedent(kscreensaver)
            )
        else:
            shutil.copy(
                os.path.expanduser("~/.config/kscreenlockerrc.offline"),
                os.path.expanduser("~/.config/kscreenlockerrc"),
            )
    else:
        raise NotImplementedError
